# MIGRATED TO [GITHUB](https://github.com/hildickethan/Web_Restaurant_New_FW_PHP_MVC_OOP_JQuery)


# Web_Restaurant_PHP_MVC_OOP_JQuery

Simple restaurant web page made with PHP, Javascript, JQuery and MVC architecture.

## Features

* Home with search bar
* CRUD
* Contact Us page with Google Maps pin
* JQWidgets datatable
* Shop Page
* Shopping cart with checkout
* Register/Login

### Technologies used

* [PHP]
* [jQuery] 
* [JavaScript]
* [MVC]


### Other technologies used
* [Datatables]
* [JQWidgets] 
* [Cloud83] - The template, created by Colorlib
* [Yelp Fusion API]
* [Google Maps API]

[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)

   [jQuery]: <http://jquery.com>
   [PHP]: <http://php.net>
   [MVC]: <https://www.tutorialspoint.com/mvc_framework/mvc_framework_introduction.htm>
   [JavaScript]: <https://www.javascript.com/>
   [Datatables]: <https://datatables.net/>
   [JQWidgets]: <https://www.jqwidgets.com/>
   [Cloud83]: <https://colorlib.com/wp/template/cloud83/>
   [Yelp Fusion API]: <https://www.yelp.com/fusion>
   [Google Maps API]: <https://developers.google.com/maps/documentation/javascript/tutorial>