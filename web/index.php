<?php
session_start();
require('model/constants.php');
require('model/session.inc.php');
require('paths.php');

$_SESSION['module']="";

if (isset($_SESSION["user"])) {  
	$_SESSION["time"] = time(); //Devuelve la fecha actual
}

regenerateSession();

//top page
if ((isset($_GET['page'])) && (($_GET['page']==="controller_restaurants"))){
	include("view/inc/top_page_restaurants.php");
} else if ((isset($_GET['page']) ) && ($_GET['page']==="controller_favourites")){
	include("view/inc/top_page_favourites.php");
}	
else{
	include("view/inc/top_page.php");
}

//header
include("view/inc/header.php");

//login window
include("components/login/view/login_window.html");

//page
// include("view/inc/pages.php"); 

if (!isset($_GET['module'])){
    include("view/inc/pages.php"); 
} else if((isset($_GET['module'])) && (!isset($_GET['view']))){
    require_once("module/".$_GET['module']."/controller/controller_" .$_GET['module'].".class.php");
}

if ((isset($_GET['module'])) && (isset($_GET['view']))){
    require_once("module/".$_GET['module']."/view/".$_GET['view'].".html");
}

//footer
include("view/inc/footer.php");


