<?php
    $path = $_SERVER['DOCUMENT_ROOT'] . '/framework/web/';
    include($path . "model/connect.class.php");
    class DAOCart{
        function get_price($data){
            $id = $data['id'];

            $sql = "SELECT price FROM restaurants WHERE id = $id";
            
            $conexion = connect::con();
            $res = mysqli_query($conexion, $sql);
            connect::close($conexion);
            return $res;
        }

        function purchase($data, $pid){
            @session_start();
            $pid = $pid['pid'];
            $rid = $data['id'];
            $uid = $_SESSION['user']['id'];
            $price = $data['price'];
            $quantity = $data['quantity'];

            $sql = "INSERT INTO purchases VALUES($pid, $rid, $uid, $price, $quantity)";

            $conexion = connect::con();
            $res = mysqli_query($conexion, $sql);
            connect::close($conexion);
            return $res;
        }

        function get_pid(){
            $sql = "SELECT MAX(pid) pid FROM purchases";
            
            $conexion = connect::con();
            $res = mysqli_query($conexion, $sql);
            connect::close($conexion);
            return $res;
        }
    }