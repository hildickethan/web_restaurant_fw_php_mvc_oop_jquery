$(function () {    
    ////////////////////////////////////////////////
    // display and hide
    ////////////////////////////////////////////////

    $('#loginbutton').on('click', function(){
        $('#login-block').fadeIn(300);
    });
    
    $('#login-block').on('click', function(event){
        if (event.target == this)
            $('#login-block').fadeOut(300);
    });

    $('.message a').on('click', function(){
        $('form').animate({height: "toggle", opacity: "toggle"}, "slow");
    });

    ////////////////////////////////////////////////
    // validation
    ////////////////////////////////////////////////

    function basic_validate(name) {
        if (name.length > 0) {
            var regexp = /^([a-zA-Z0-9]{4,16})$/;
            return regexp.test(name);
        }
        return false;
    }

    function email_validate(email) {
        if (email.length > 0) {
            var regexp = /^([a-zA-Z0-9]{2,20}@[a-zA-Z]{2,12}\.[a-z]{2,3})$/; // very basic email check
            return regexp.test(email);
        }
        return false;
    }
    

    function validate_form(form, origin) {
        var result = true;
        // console.log(form);

        $.each(form, function(index){
            if (index == 3){ //email
                if (!email_validate(form[index].value)) {
                    document.getElementById(form[index].name).classList.add('is-not-valid');
                    result = false;
                } else
                    document.getElementById(form[index].name).classList.remove('is-not-valid');
            } else {
                if (!basic_validate(form[index].value)) {
                    document.getElementById(form[index].name).classList.add('is-not-valid');
                    result = false;
                } else
                    document.getElementById(form[index].name).classList.remove('is-not-valid');
            }
        });
        if (origin == "register"){
            // password match
            if (result){ //if everything is still valid
                if (form[1].value == form[2].value)
                    result = true;
                else{
                    document.getElementById(form[2].name).classList.add('is-not-valid');
                    result = false;                
                }
            }
        }
        return result;
    }

    ////////////////////////////////////////////////
    // register
    ////////////////////////////////////////////////

    $('#usernameregister,#passwordregister,#password2register,#emailregister').on('keyup', function(e){
        //e.keyCode == 13
        if (e.key === "Enter"){
            register();
        }
    });

    $('#registerbutton').on('click', function(){
           register();
    });

    function register(){
        var form = $('#register-form').serializeArray();
        if (validate_form(form, 'register')){
            form.push({"name":"avatar", "value": `https://api.adorable.io/avatars/256/${form[0].value}`});
            console.log(form);
            
            $.ajax({
                data: form,
                url: "components/login/controller/controller_login.php?op=register",
                type: 'POST',
                success: function(data){
                    // data = JSON.parse(data);
                    console.log(data);
                    switch (data) {
                        case 'Username already exists':
                            document.getElementById('usernameregister').classList.add('is-not-valid');
                            document.getElementById('emailregister').classList.remove('is-not-valid');
                            break;

                        case 'Email already exists':
                            document.getElementById('usernameregister').classList.remove('is-not-valid');
                            document.getElementById('emailregister').classList.add('is-not-valid');
                            break;

                        case 'Inserted':
                            // $('#login-block').fadeOut(300);
                            // $('#header-btns').hide();
                            // $("#logged-in-avatar").attr("src",form[4].value);
                            // $('#logged-in').show();
                            document.getElementById('usernameregister').classList.remove('is-not-valid');
                            document.getElementById('emailregister').classList.remove('is-not-valid');
                            break;
                        
                        default:
                            break;
                    }
                }
            });
        }     
    }

    ////////////////////////////////////////////////
    // login
    ////////////////////////////////////////////////

    
    $('#usernamelogin,#passwordlogin').on('keyup', function(e){
        if (e.key === "Enter"){
            login();
        }
    });

    $('#loginformbutton').on('click', function(){
        login();
    });

    $('#logged-in-avatar').on('click', function(){
        $.ajax({
            url: "components/login/controller/controller_login.php?op=logout",
            type: 'POST',
            success: function(){
                window.location.href = "index.php";
            }
        });
    });

    function checkLogin(){
        $.ajax({
            url: "components/login/controller/controller_login.php?op=checklogin",
            type: 'GET',
            success: function(data){
                data = JSON.parse(data);
                // console.log(data);
                $('#controller_restaurants').hide();
                $('#controller_favourites').hide();
                $('#logged-in').hide();

                if (data != 'notlogged'){
                    $('#logged-in-avatar').attr('src',data['avatar']);
                    $('#logged-in-name').html(data['username']);
                    // console.log(Boolean(+data['admin']));

                    if (Boolean(+data['admin'])){
                        $('#controller_restaurants').show();
                    }

                    $('#controller_favourites').show();
                    $('#logged-in').show();

                    $('#header-btns').hide();                    
                }
            }
        });
    }

    function login(){
        var form = $('#login-form').serializeArray();
        // console.log(form);
        
        if(validate_form(form, 'login')){
            $.ajax({
                data: form,
                url: "components/login/controller/controller_login.php?op=login",
                type: 'POST',
                success: function(data){
                    // console.log(data);
                    if (!data){
                        window.location.href = "index.php";
                    }else {
                        data = JSON.parse(data);
                        if (data == "nouser"){
                            document.getElementById('usernamelogin').classList.add('is-not-valid');
                            document.getElementById('passwordlogin').classList.remove('is-not-valid');
                        } else if (data == "badpw"){
                            document.getElementById('usernamelogin').classList.remove('is-not-valid');
                            document.getElementById('passwordlogin').classList.add('is-not-valid');
                        }

                    }
                    
                }
            });
        }
    }

    ////////////////////////////////////////////////
    // on load
    ////////////////////////////////////////////////

    checkLogin();

    setInterval(function(){
		$.ajax({
			type : 'GET',
			url  : 'components/login/controller/controller_login.php?&op=activity',
			success :  function(response){	
                // console.log(Boolean(+response));				
				if(Boolean(+response)){
					// alert("Se ha cerrado la cuenta por inactividad")
					$.ajax({
                        url: "components/login/controller/controller_login.php?op=logout",
                        type: 'POST',
                        success: function(){
                            window.location.href = "index.php";
                        }
                    });
				}
			}
		});
	}, 60000);
});
