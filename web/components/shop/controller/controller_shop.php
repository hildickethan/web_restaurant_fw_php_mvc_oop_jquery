<?php 
	$path = $_SERVER['DOCUMENT_ROOT'] . '/framework/web/';
    include($path . "components/shop/model/DAO_shop.php");
	//@session_start();
    switch ($_GET['op']) {
        case 'list':
            try{
                $daoshop = new DAOShop();
                $rdo = $daoshop->select_shop_restaurants();
            }catch (Exception $e){
                $callback = 'index.php?page=503';
                die('<script>window.location.href="'.$callback .'";</script>');
            }
            
            if(!$rdo){
                $callback = 'index.php?page=503';
                die('<script>window.location.href="'.$callback .'";</script>');
            }else{
                include($path . "components/shop/view/list_shop.php");
            }
            break;

        case 'autocomplete':
            if (isset($_SESSION['tolist'])) {
                $_POST = $_SESSION['tolist'];
                session_unset();
            }

            try{
                $daoshop = new DAOShop();
                $rdo = $daoshop->select_autocomplete($_POST);
                
            }catch (Exception $e){
                $callback = 'index.php?page=503';
                die('<script>window.location.href="'.$callback .'";</script>');
            }
            
            if(!$rdo){
                echo json_encode("error");
            }
            else{
                $dinfo = array();
                foreach ($rdo as $row) {
                    array_push($dinfo, $row);
                }
                echo json_encode($dinfo);
            }
        break;

        case 'search':
            try{
                $daoshop = new DAOShop();
                $rdo = $daoshop->select_search($_POST);
                
            }catch (Exception $e){
                $callback = 'index.php?page=503';
                die('<script>window.location.href="'.$callback .'";</script>');
            }
            
            if(!$rdo){
                echo json_encode("error");
            }
            else{
                $dinfo = array();
                foreach ($rdo as $row) {
                    array_push($dinfo, $row);
                }
                echo json_encode($dinfo);
            }
        break;

        case 'redirectlist':
            session_start();
            $_SESSION['tolist'] = $_POST;
            // echo json_encode($_SESSION['tolist']);
            break;
            
        case 'redirectdetails':
            session_start();
            $_SESSION['details'] = $_POST;
            echo json_encode($_SESSION['details']);
            break;

        case 'details':
            include($path . "components/shop/view/details_shop.php");
            break;
        
        case 'loaddetails':
            session_start();
            echo json_encode($_SESSION['details']);
            break;

		default:
			include("view/inc/error404.php");
			break;
	}
