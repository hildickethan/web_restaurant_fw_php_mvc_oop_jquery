<div id="preloder">
    <div class="loader"></div>
</div>

<header class="header-section">
    <div class="container">
        <a href="index.html" class="site-logo">
            <img src="view/img/logo.png" alt="logo">
        </a>
        <!-- Switch button -->
        <div class="nav-switch">
            <div class="ns-bar"></div>
        </div>
        <div class="header-right">
                <a class="shopping-cart" id="cartbutton" href="index.php?page=controller_cart&op=list"><img src="view/img/cart.png" /></a>
                <div class="cart-number" id="cart-number"></div>
            <ul class="main-menu">
                <li id="homepage"><a href="index.php?page=homepage" data-tr="Home"></a></li>
                <li id="controller_restaurants"><a href="index.php?page=controller_restaurants&op=list">Restaurants</a></li>
                <li id="controller_favourites"><a href="index.php?page=controller_favourites&op=list">Favourites</a></li>
                <li id="aboutus"><a href="index.php?page=aboutus">About Us</a></li>
                <li id="services"><a href="index.php?page=services">Services</a></li>
                <li id="contactus"><a href="index.php?page=contactus">Contact us</a></li>
                <li id="controller_shop"><a href="index.php?page=controller_shop&op=list">Shop</a></li>
            </ul>
            <div class="header-btns" id="header-btns">
                <a type="button" id="loginbutton" class="site-btn sb-c2">Login</a>
            </div>
            <div class="logged-in" id="logged-in">
                <h3 id="logged-in-name"></h3>
                <img id="logged-in-avatar" src="#"/>
            </div>
        </div>
    </div>
</header>