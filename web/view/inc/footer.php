<footer class="footer-section">
	<div class="container">
		<div class="footer-nav">
			<ul>
				<li><a href="index.php?page=homepage" data-tr="Home"></a></li>
                <li><a href="index.php?page=controller_restaurants&op=list">Restaurants</a></li>
                <li><a href="index.php?page=controller_favourites&op=list">Favourites</a></li>
                <li><a href="index.php?page=aboutus">About Us</a></li>
                <li><a href="index.php?page=services">Services</a></li>
                <li><a href="index.php?page=contactus">Contact us</a></li>
			</ul>
		</div>
		<div id="idioma" class="lang">
			<span data-tr="Change language:"></span>
			<select name="select-lang" id="select-lang" class="custom-select">
				<option data-tr="English"></option>
				<option data-tr="Spanish"></option>
				<option data-tr="Valencian"></option>
			</select>
		</div>
		<div class="copyright">
			<p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
			Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made by <a href="https://colorlib.com" target="_blank">Colorlib</a>
			<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
			</p>
		</div>
	</div>
</footer>


<!--====== Javascripts & Jquery ======-->
<script src="view/js/bootstrap.min.js"></script>
<script src="view/js/owl.carousel.min.js"></script>
<script src="view/js/jquery.magnific-popup.min.js"></script>
<script src="view/js/circle-progress.min.js"></script>
<script src="view/js/main.js"></script>
<script src="components/shop/model/searchbar.js"></script>
<script src="components/login/model/login.js"></script>
<script src="components/cart/model/cart.js"></script>
<script src="module/profile/view/js/profile.js"></script>
<script src="view/js/dropzone.js"></script>

</body>
</html>