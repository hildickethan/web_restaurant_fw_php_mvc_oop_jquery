<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title data-tr="Create restaurant"></title>

		<!-- My css-->
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.css" />
		
		<!-- My links -->
		<script type="text/javascript" src="view/js/translate.js"></script>
    	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    	<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.js"></script>
    	<script type="text/javascript" src="view/js/datepicker.js"></script>
	    <script src="module/restaurants/model/validate_restaurants.js"></script>
		<script src="module/restaurants/view/js/restaurants.js"></script>

        <!-- dataTables -->
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" />
		<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
		
		<!-- Favicon -->
        <link href="view/img/favicon.ico" rel="shortcut icon"/>
        
        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css?family=Poppins:400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
        
        <!-- Stylesheets -->
        <link rel="stylesheet" href="view/css/bootstrap.min.css"/>
        <link rel="stylesheet" href="view/css/font-awesome.min.css"/>
        <link rel="stylesheet" href="view/css/magnific-popup.css"/>
        <link rel="stylesheet" href="view/css/owl.carousel.min.css"/>
        <link rel="stylesheet" href="view/css/style.css"/>
        <link rel="stylesheet" href="view/css/animate.css"/>
        
	    <!-- My css-->
		<link href="view/css/custom.css" rel="stylesheet" type="text/css" />
		
    </head>
    <body>