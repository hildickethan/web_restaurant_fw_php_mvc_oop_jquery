<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title data-tr="Favourite restaurants"></title>

		<!-- My links -->
    	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0-beta1/jquery.js"></script>
        <script type="text/javascript" src="view/js/translate.js"></script>
        
        <!-- favourite specific -->
        <script type="text/javascript" src="module/favourites/model/filter_page.js" ></script>

        <!-- Favicon -->
        <link href="view/img/favicon.ico" rel="shortcut icon"/>
        
        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css?family=Poppins:400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
        
        <!-- Stylesheets -->
        <link rel="stylesheet" href="view/css/bootstrap.min.css"/>
        <link rel="stylesheet" href="view/css/font-awesome.min.css"/>
        <link rel="stylesheet" href="view/css/magnific-popup.css"/>
        <link rel="stylesheet" href="view/css/owl.carousel.min.css"/>
        <link rel="stylesheet" href="view/css/style.css"/>
        <link rel="stylesheet" href="view/css/animate.css"/>
        
	    <!-- My css-->
		<link href="view/css/custom.css" rel="stylesheet" type="text/css" />

		<!-- jqwidgets -->
            <!-- css -->
		    <link rel="stylesheet" href="view/js/jqwidgets/jqwidgets/styles/jqx.base.css" type="text/css"/>
            <link rel="stylesheet" href="view/js/jqwidgets/jqwidgets/styles/jqx.metro.css" type="text/css"/>

            <!-- js -->
            <script type="text/javascript" src="view/js/jqwidgets/jqwidgets/jqxcore.js"></script>
            <script type="text/javascript" src="view/js/jqwidgets/jqwidgets/jqxbuttons.js"></script>
            <script type="text/javascript" src="view/js/jqwidgets/jqwidgets/jqxscrollbar.js"></script>
            <script type="text/javascript" src="view/js/jqwidgets/jqwidgets/jqxdata.js"></script> 
            <script type="text/javascript" src="view/js/jqwidgets/jqwidgets/jqxdatatable.js"></script> 
            <script type="text/javascript" src="view/js/jqwidgets/jqwidgets/jqxlistbox.js"></script>
            <script type="text/javascript" src="view/js/jqwidgets/jqwidgets/jqxdropdownlist.js"></script>

            <!-- scripts -->
            <script type="text/javascript" src="view/js/jqwidgets/scripts/demos.js"></script>
		
    </head>
    <body>