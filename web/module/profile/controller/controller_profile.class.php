<?php
@session_start();

include($_SERVER['DOCUMENT_ROOT'] . "/framework/web/utils/utils.inc.php");
include($_SERVER['DOCUMENT_ROOT'] . "/framework/web/utils/upload.php");

if ((isset($_GET["upload"])) && ($_GET["upload"] == true)){
    $result_prodpic = upload_files();
    $_SESSION['result_prodpic'] = $result_prodpic;
    echo json_encode($result_prodpic);
}
if ((isset($_GET["delete"])) && ($_GET["delete"] == true)){
  // echo json_encode("Hello world from delete in controller_profile.class.php");
  $_SESSION['result_prodpic'] = array();
  $result = remove_files();
  if($result === true){
    echo json_encode(array("res" => true));
  }else{
    echo json_encode(array("res" => false));
  }
  //echo json_decode($result);
}