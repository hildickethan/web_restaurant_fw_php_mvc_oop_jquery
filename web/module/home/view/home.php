<!-- Hero section -->
<section class="hero-section">
	<div class="hero-slider owl-carousel">
		<div class="hs-item set-bg" data-setbg="view/img/bgrestaurant.jpg">
			<div class="container">
				<h2>The Best Restaurants</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec malesuada lorem maximus mauris sceleri sque,</p>
				<p>at rutrum nulla dictum. Ut ac ligula sapien. Suspendisse cursus faucibus finibus.</p>
				<div class="clearfix"></div>
				<a href="" class="site-btn sb-c1">Read More</a>
			</div>
		</div>
		<div class="hs-item set-bg" data-setbg="view/img/bgrestaurant2.jpg">
			<div class="container">
				<h2>The Best Restaurants</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec malesuada lorem maximus mauris sceleri sque,</p>
				<p>at rutrum nulla dictum. Ut ac ligula sapien. Suspendisse cursus faucibus finibus.</p>
				<div class="clearfix"></div>
				<a href="" class="site-btn sb-c1">Read More</a>
			</div>
		</div>
	</div>
</section>
<!-- Hero section end -->


<!-- Feature section -->
<section class="feature-section spad">
	<input class="site-btn sb-c1 searchbutton" type="button" id="searchbutton_home" value="Search">
	<form class="row search-bar-restaurants" id="searchform" name="searchform">
		<!-- <input type="select" placeholder="Type" id="searchtype" list="typelist"> -->
		<!-- <datalist id="typelist">
			<option value="Gourmet">
			<option value="Fast Food">
			<option value="Ethnic">
		</datalist> -->
		<select class="custom-select" id="searchtype">
			<option>Type</option>
			<option>Gourmet</option>
			<option>Fast Food</option>
			<option>Ethnic</option>
		</select>

		<div class="inputwrap">
			<input type="text" placeholder="Tastes" id="searchtastes">
			<div class="dropdown">
				<div id="dropdown-tastes" class="dropdown-content">
				
				</div>
			</div>
		</div>
		<div class="inputwrap">
			<input type="text" placeholder="Restaurant name" id="searchname">
				<div class="dropdown">
					<div id="dropdown-name" class="dropdown-content">
					
					</div>
				</div>
		</div>
	</form>
	<div class="container">


		<div class="row">
			<?php
				if ($rdo->num_rows === 0){
					echo '<h4>NO RESTAURANTS</h4>';
				} else{
					$imgnum=1;
					foreach ($rdo as $row) {
						echo '<div class="col-md-4 feature">';
						echo '<img src="view/img/restaurant'.$imgnum.'.jpg" alt="#">';
						echo '<h4>'. $row['name'] . '</h4>';
						echo '</div>';
						if ($imgnum >= 3)
							$imgnum = 1;
						else 
							$imgnum++;
					}
				}
			?>
		</div>
	</div>
</section>
<!-- Feature section end -->