<!-- Page Top section -->
<section class="page-top-section set-bg" data-setbg="view/img/bgrestaurant.jpg">
    <div class="container">
        <h2>About Us</h2>
    </div>
</section>
<!-- Page Top section end -->

<!-- About Intro section -->
<section class="about-intro-section spad">
    <div class="container">
        <div class="section-title">
            <p>The only ones</p>
            <h2>We Generate Trust</h2>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="about-intro-pic">
                    <img src="view/img/about-intro.jpg" alt="">
                    <a href="https://www.youtube.com/watch?v=4T31zr0fsvs" class="video-play video-popup"><i class="fa fa-play"></i></a>
                </div>
            </div>
            <div class="col-lg-6 about-intro-text">
                <p>Phasellus vehicula tempus orci vel consequat. Nullam lorem sem, viverra a rutrum sed, gravida mattis magna. Suspendisse vitae commodo quam. Quisque a enim et ante vulputate finibus nec laoreet ipsum. Etiam lobortis erat vel ullamcorper tristique. Morbi pellentesque orci augue, quis finibus justo accumsan nec. Donec imperdiet sodales lectus sed consectetur. Mauris dignissim.Morbi pellentesque orci augue, quis finibus justo accumsan nec. Donec imperdiet sodales lectus sed consectetur. Mauris dignissim</p>
                <div class="site-btn sb-c3">Read more</div>
            </div>
        </div>
    </div>
</section>
<!-- About Intro section end -->

<!-- Testimonials section -->
<!--<section class="testimonials-section">
    <div class="container">
        <div class="section-title">
            <p>The only ones</p>
            <h2>Testimonials</h2>
        </div>
        <div class="row">
            <div class="col-lg-10 offset-lg-1">
                <div class="testimonials-slider owl-carousel">
                    <div class="testimonial-item">
                        <p>Phasellus vehicula tempus orci vel consequat. Nullam lorem sem, viverra a rutrum sed, gravida mattis magna. Suspendisse vitae commodo quam. Quisque a enim et ante vulputate finibus nec laoreet ipsum. Etiam lobortis erat vel ullamcorper tristique. Morbi pellentesque orci augue, quis finibus justo accumsan nec. Donec imperdiet sodales lectus sed consectetur. Mauris dignissim.Morbi pellentesque orci augue, quis finibus justo accumsan nec. Donec imperdiet sodales lectus sed consectetur. Mauris dignissim</p>
                        <div class="ti-author-pic set-bg" data-setbg="view/img/testimonials/1.jpg"></div>
                        <h5>Maria Smith, Client</h5>
                    </div>
                    <div class="testimonial-item">
                        <p>Phasellus vehicula tempus orci vel consequat. Nullam lorem sem, viverra a rutrum sed, gravida mattis magna. Suspendisse vitae commodo quam. Quisque a enim et ante vulputate finibus nec laoreet ipsum. Etiam lobortis erat vel ullamcorper tristique. Morbi pellentesque orci augue, quis finibus justo accumsan nec. Donec imperdiet sodales lectus sed consectetur. Mauris dignissim.Morbi pellentesque orci augue, quis finibus justo accumsan nec. Donec imperdiet sodales lectus sed consectetur. Mauris dignissim</p>
                        <div class="ti-author-pic set-bg" data-setbg="view/img/testimonials/1.jpg"></div>
                        <h5>Maria Smith, Client</h5>
                    </div>
                    <div class="testimonial-item">
                        <p>Phasellus vehicula tempus orci vel consequat. Nullam lorem sem, viverra a rutrum sed, gravida mattis magna. Suspendisse vitae commodo quam. Quisque a enim et ante vulputate finibus nec laoreet ipsum. Etiam lobortis erat vel ullamcorper tristique. Morbi pellentesque orci augue, quis finibus justo accumsan nec. Donec imperdiet sodales lectus sed consectetur. Mauris dignissim.Morbi pellentesque orci augue, quis finibus justo accumsan nec. Donec imperdiet sodales lectus sed consectetur. Mauris dignissim</p>
                        <div class="ti-author-pic set-bg" data-setbg="view/img/testimonials/1.jpg"></div>
                        <h5>Maria Smith, Client</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>-->
<!-- Testimonials section end -->