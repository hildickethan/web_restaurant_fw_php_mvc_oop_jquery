<?php
	$path = $_SERVER['DOCUMENT_ROOT'] . '/framework/web/';
	include($path . "model/connect.class.php");
	
	class DAORestaurants{
		function insert_restaurant($data){
			$name=$data['Name'];
        	$type=$data['Type'];
        	$people=$data['People'];
        	$date=$data['Date'];
			$tastes=implode(",",$data['Tastes']);
			
        	$sql = " INSERT INTO restaurants (name, type, people, selected_date, tastes)"
        		. " VALUES ('$name', '$type', '$people', '$date', '$tastes')";
            
            $conexion = connect::con();
            $res = mysqli_query($conexion, $sql);
            connect::close($conexion);
			return $res;
		}
		
		function select_all_restaurants(){
			$sql = "SELECT * FROM restaurants ORDER BY id ASC";
			
			$conexion = connect::con();
            $res = mysqli_query($conexion, $sql);
            connect::close($conexion);
            return $res;
		}
		
		function select_restaurant($id){
			$sql = "SELECT * FROM restaurants WHERE id='$id'";
			
			$conexion = connect::con();
            $res = mysqli_query($conexion, $sql)->fetch_object();
            connect::close($conexion);
            return $res;
		}
		
		function update_restaurant($data){
			$id=$data['id'];
			$name=$data['Name'];
        	$type=$data['Type'];
        	$people=$data['People'];
        	$date=$data['Date'];
			$tastes=implode(",",$data['Tastes']);
        	
			$sql = " UPDATE restaurants SET id='$id', name='$name', type='$type', people=$people,"
			. " selected_date='$date', tastes='$tastes' WHERE id=$id";
            
            $conexion = connect::con();
            $res = mysqli_query($conexion, $sql);
            connect::close($conexion);
			return $res;
		}
		
		function delete_restaurant($id){
			$sql = "DELETE FROM restaurants WHERE id='$id'";
			
			$conexion = connect::con();
            $res = mysqli_query($conexion, $sql);
            connect::close($conexion);
            return $res;
		}

		function delete_all_restaurants(){
			$sql = "DELETE FROM restaurants";
			
			$conexion = connect::con();
            $res = mysqli_query($conexion, $sql);
            connect::close($conexion);
            return $res;
		}
	}