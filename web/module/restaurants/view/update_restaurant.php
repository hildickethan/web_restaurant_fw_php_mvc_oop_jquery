<!-- Page Top section -->
<section class="page-top-section set-bg" data-setbg="view/img/bgrestaurant.jpg">
    <div class="container">
        <h2>Modify restaurant</h2>
    </div>
</section>

<!-- Page Top section end -->

<section class="contact-section spad">
  <div class="container">
    <div class="row">
      <div class="col-lg-6">
        <form class="contact-form" autocomplete="on" method="POST" name="aupdate_restaurant" id="update_restaurant">
          <input name="id" type="text" id="id" placeholder="ID" value="<?php echo $restaurant['id'];?>" readonly>
          <input name="Name" type="text" id="name" placeholder="Name" value="<?php echo $restaurant['name'];?>"> <span id="e_name"></span>
          <p>Restaurant type
            <select name="Type" id="type">
              <option value="Gourmet"<?php if ($restaurant['type']== "Gourmet") echo 'selected="selected"'; ?>>Gourmet</option>
                    <option value="Fast Food" <?php if ($restaurant['type']== "Fast Food") echo 'selected="selected"'; ?>>Fast Food</option>
                    <option value="Ethnic" <?php if ($restaurant['type']== "Ethnic") echo 'selected="selected"'; ?>>Ethnic</option>
            </select>
          </p> <p id="e_type"></p>
          <p>
             Number of people
                1 <input name="People" id="p1" type="radio" value="1" <?php if ($restaurant['people']== 1) echo 'checked="checked"'; ?>>
                2 <input name="People" id="p2" type="radio" value="2" <?php if ($restaurant['people']== 2) echo 'checked="checked"'; ?>>
                3 <input name="People" id="p3" type="radio" value="3" <?php if ($restaurant['people']== 3) echo 'checked="checked"'; ?>>
                4 <input name="People" id="p4" type="radio" value="4" <?php if ($restaurant['people']== 4) echo 'checked="checked"'; ?>>
                5 <input name="People" id="p5" type="radio" value="5" <?php if ($restaurant['people']== 5) echo 'checked="checked"'; ?>>
                6 <input name="People" id="p6" type="radio" value="6" <?php if ($restaurant['people']== 6) echo 'checked="checked"'; ?>>
          </p><p id="e_people"></p>
          <input id="datepicker" type="text" name="Date" placeholder="Date" value="<?php echo $restaurant['selected_date'];?>"><p id="e_date"></p>
          <p>
            Tastes 
            Meat <input type="checkbox" id="t1" name="Tastes[]" value="Meat"<?php if (strpos($restaurant['tastes'], 'Meat') !== false) echo 'checked="checked"'; ?>>
            Fish  <input type="checkbox" id="t2" name="Tastes[]" value="Fish" <?php if (strpos($restaurant['tastes'], 'Fish') !== false) echo 'checked="checked"'; ?>>
            Vegetarian <input type="checkbox" id="t3" name="Tastes[]" value="Vegetarian" <?php if (strpos($restaurant['tastes'], 'Vegetarian') !== false) echo 'checked="checked"'; ?>>      
          </p><p id="e_tastes"></p>
          <input class="site-btn sb-c1" type="button" onclick="validate_restaurant()" name="update" id="update" value="Submit">
          <a href="index.php?page=controller_restaurants&op=list">Back</a>
        </form>
      </div>
    </div>
  </div>
</section>