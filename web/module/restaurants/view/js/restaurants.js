$(document).ready(function () {
    $("#restaurant_modal").hide();
    

    $('#table_crud').DataTable();
    // $('#p1').jqxRadioButton({ width: 120, height: 25 });
    // $("#p1").bind('change', function (event) {
        // var checked = event.args.checked;
        // alert('p1 checked: ' + checked);
    // });
    
    

    $('.restaurant').on('click', function () {
        var id = this.getAttribute('id');
      //  console.log(id);

        $.ajax({
            type: 'GET',
            url: 'module/restaurants/controller/controller_restaurants.php?op=read_modal&modal=' + id,
            success: function(data) {
                data = JSON.parse(data);
                //console.log(data);
                if(data === 'error') 
                    window.location.href='index.php?page=503';
                else {
                    $("#id").html(data.id);
                    $("#name").html(data.name);
                    $("#type").html(data.type);
                    $("#people").html(data.people);
                    $("#date").html(data.date);
                    $("#tastes").html(data.tastes);
            
                    $("#details_restaurant").show();
                    $("#restaurant_modal").dialog({
                        width: 850, //<!-- ------------- ancho de la ventana -->
                        height: 500, //<!--  ------------- altura de la ventana -->
                        //show: "scale", <!-- ----------- animación de la ventana al aparecer -->
                        //hide: "scale", <!-- ----------- animación al cerrar la ventana -->
                        resizable: "false", //<!-- ------ fija o redimensionable si ponemos este valor a "true" -->
                        //position: "down",<!--  ------ posicion de la ventana en la pantalla (left, top, right...) -->
                        modal: "true", //<!-- ------------ si esta en true bloquea el contenido de la web mientras la ventana esta activa (muy elegante) -->
                        buttons: {
                            Ok: function () {
                                $(this).dialog("close");
                            }
                        },
                        show: {
                            effect: "blind",
                            duration: 200
                        },
                        hide: {
                            effect: "explode",
                            duration: 200
                        }
                    });
                }
            },
            error: function(data) {
                console.log('------ERROR------');
                console.log(data);
            }
        });

    });    
});
