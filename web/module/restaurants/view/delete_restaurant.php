<!-- Page Top section -->
<section class="page-top-section set-bg" data-setbg="view/img/bgrestaurant.jpg">
    <div class="container">
        <h2>Delete restaurant</h2>
    </div>
</section>

<!-- Page Top section end -->

<section class="contact-section spad">
  <div class="container">
    <div class="row">
      <div class="col-lg-10">
        <form class="contact-form" style="margin-left: 30%;" autocomplete="on" method="POST" name="delete_restaurant" id="delete_restaurant" action="index.php?page=controller_restaurants&op=delete&id=<?php echo $_GET['id']; ?>">
            <table border='0'>
                <tr>
                    <td align="center"  colspan="2"><h3>Do you wish to delete restaurant <?php echo $_GET['name']; ?>?</h3></td>
                    
                </tr>
                <tr>
                    <td align="center"><button type="submit" class="Button_green"name="delete" id="delete">Accept</button></td>
                    <td align="center"><a class="Button_red" href="index.php?page=controller_restaurants&op=list">Cancel</a></td>
                </tr>
            </table>
        </form>
      </div>
    </div>
  </div>
</section>