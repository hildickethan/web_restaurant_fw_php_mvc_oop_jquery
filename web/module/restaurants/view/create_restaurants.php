<!-- Page Top section -->
<section class="page-top-section set-bg" data-setbg="view/img/bgrestaurant.jpg">
    <div class="container">
        <h2>Create restaurant</h2>
    </div>
</section>
<!-- Page Top section end -->

<!-- Services section -->
<section class="contact-section spad">
  <div class="container">
    <div class="row">
      <div class="col-lg-6">
        <form class="contact-form" name="formrestaurants" method="POST" id="formrest">
          <input name="Name" type="text" id="name" placeholder="Name"> <span id="e_name"></span>
          <p>Restaurant type
            <select name="Type" id="type">
              <option value="Gourmet">Gourmet</option>
              <option value="Fast Food">Fast Food</option>
              <option value="Ethnic">Ethnic</option>
            </select>
          </p> <p id="e_type"></p>
          <p>
             Number of people
              1 <input name="People" id="p1" type="radio" value="1">
              2 <input name="People" id="p2" type="radio" value="2">
              3 <input name="People" id="p3" type="radio" value="3">
              4 <input name="People" id="p4" type="radio" value="4">
              5 <input name="People" id="p5" type="radio" value="5">
              6 <input name="People" id="p6" type="radio" value="6">
          </p><p id="e_people"></p>
          <input id="datepicker" type="text" name="Date" placeholder="Date"><p id="e_date"></p>
          <p>
            Tastes 
            Meat <input type="checkbox" id="t1" name="Tastes[]" value="Meat">
            Fish  <input type="checkbox" id="t2" name="Tastes[]" value="Fish">
            Vegetarian <input type="checkbox" id="t3" name="Tastes[]" value="Vegetarian">      
          </p><p id="e_tastes"></p>
          <input class="site-btn sb-c1" type="button" onclick="validate_restaurant()" name="Submit" value="Submit">
          <a href="index.php?page=controller_restaurants&op=list">Back</a>
        </form>
      </div>
    </div>
  </div>
</section>
<!-- Services section end -->