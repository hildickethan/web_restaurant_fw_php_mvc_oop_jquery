<div id="contenido">
    <h3>Restaurant information</h3>
    <p>
    <table border='1'>
        <tr>
            <td>Restaurant id: </td>
            <td>
                <?php
                    echo $restaurant['id'];
                ?>
            </td>
        </tr>
    
        <tr>
            <td>Restaurant name: </td>
            <td>
                <?php
                    echo $restaurant['name'];
                ?>
            </td>
        </tr>
        
        <tr>
            <td>Restaurant type: </td>
            <td>
                <?php
                    echo $restaurant['type'];
                ?>
            </td>
        </tr>
        
        <tr>
            <td>Amount of people: </td>
            <td>
                <?php
                    echo $restaurant['people'];
                ?>
            </td>
        </tr>
        
        <tr>
            <td>Date: </td>
            <td>
                <?php
                    echo $restaurant['selected_date'];
                ?>
            </td>
        </tr>
        
        <tr>
            <td>Tastes: </td>
            <td>
                <?php
                    echo $restaurant['tastes'];
                ?>
            </td>
        </tr>
    </table>
    </p>
    <p><a href="index.php?page=controller_restaurants&op=list">Back</a></p>
</div>