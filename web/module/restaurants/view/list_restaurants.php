<!-- Page Top section -->
<section class="page-top-section set-bg" data-setbg="view/img/bgrestaurant.jpg">
    <div class="container">
        <h2>Restaurant list</h2>
    </div>
</section>
<!-- Page Top section end -->

<div id="list">
    <div class="container">

            <div>
                <a href="index.php?page=controller_restaurants&op=create" class="site-btn sb-c3">Create</a>
                <a href="index.php?page=controller_restaurants&op=deleteall" class="site-btn sb-red">Delete all</a>
            </div>

    		<table id="table_crud">
                <thead>
                    <tr>
                        <td width=125><b>Restaurant ID</b></td>
                        <td width=300><b>Name</b></td>
                        <td width=125><b>Date</b></td>
                        <td width=365><b>Actions</b></td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        if ($rdo->num_rows === 0){
                            echo '<tr>';
                            echo '<td align="center"  colspan="3">NO RESTAURANTS</td>';
                            echo '</tr>';
                        }else{
                            foreach ($rdo as $row) {
                                echo '<tr>';
                                echo '<td width=125 align="center">'. $row['id'] . '</td>';
                                echo '<td width=300>'. $row['name'] . '</td>';
                                echo '<td width=125 align="center">'. $row['selected_date'] . '</td>';
                                echo '<td width=365>';
                                
                                echo "<div class='restaurant Button_purple' id='".$row['id']."'>Read</div>";
                                
                                //echo '<a class="Button_blue" href="index.php?page=controller_restaurants&op=read&id='.$row['id'].'">Read</a>';
                                echo '&nbsp;';
                                echo '<a class="Button_green" href="index.php?page=controller_restaurants&op=update&id='.$row['id'].'">Update</a>';
                                echo '&nbsp;';
                                echo '<a class="Button_red" href="index.php?page=controller_restaurants&op=delete&id='.$row['id'].'&name='.$row['name'].'">Delete</a>';
                                echo '</td>';
                                echo '</tr>';
                            }
                        }
                    ?>
                </tbody>
            </table>
    	
    </div>
</div>

<!-- modal window -->
<section id="restaurant_modal">
    <div id="details_restaurant">
        <div id="details">
            <div id="container">
                Restaurant ID: <div id="id"></div></br>
                Name: <div id="name"></div></br>
                Type: <div id="type"></div></br>
                People: <div id="people"></div></br>
                Date: <div id="date"></div></br>
                Tastes: <div id="tastes"></div></br>
            </div>
        </div>
    </div>
</section>
