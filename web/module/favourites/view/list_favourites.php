<!-- Page Top section -->
<section class="page-top-section set-bg" data-setbg="view/img/bgrestaurant.jpg">
    <div class="container">
        <h2>Restaurant list</h2>
    </div>
</section>
<!-- Page Top section end -->

<div class="container">
    <div id="dataTable"></div>
</div>